package pl.craftserwery.cslist.Commands;

import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.net.URI;
import java.net.http.HttpClient;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import pl.craftserwery.cslist.Main;

public class LinkCommand implements CommandExecutor {
	private final Main plugin;
	Map<String, Date> cooldown = new HashMap<>();
	
    public LinkCommand(Main plugin) {
        this.plugin = plugin;
    }
	
	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		Runnable runnable = () -> {
			Player player = (Player)sender;
			String name = player.getName();
			boolean requiredPermission = plugin.getConfig().getBoolean("requiredPermission");
	
			String requiredPermissionMessage = plugin.getConfig().getString("messages.needsPermissions");
			
			if (requiredPermission && !player.hasPermission("cslist.reward")) {
	            player.sendMessage(requiredPermissionMessage);
	            return;
	        }
			
			if (cooldown.containsKey(name)) {
				String cooldownMessage = plugin.getConfig().getString("messages.cooldownMessage");
				
	            Date date = cooldown.get(name);
	            long dateDiff = (long) Math.floor((new Date().getTime() / 1000) - (date.getTime() / 1000));
	
	            if (dateDiff < 15) {
	                long timeLeft = 15 - dateDiff;
	                sender.sendMessage(cooldownMessage.replace("%timeLeft%", Long.toString(timeLeft)));
	
	                return;
	            }
	        }
	
			String authKey = plugin.getConfig().getString("authKey");

			String infoMessageMessage = plugin.getConfig().getString("messages.infoMessage");
			String linkErrorMessage = plugin.getConfig().getString("messages.error");
			
			cooldown.put(name, new Date());
			
			HttpRequest request = HttpRequest.newBuilder()
					.uri(URI.create(this.plugin.apiUrl + "/api/server/vote-link"))
					.method("POST", HttpRequest.BodyPublishers.noBody())
					.version(HttpClient.Version.HTTP_1_1)
					.header("Content-Type", "application/json")
					.header("CS-Auth-Key", authKey)
					.build();
			
			HttpResponse<String> response = null;
			try {
				response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
				int statusCode = response.statusCode();
				
				if(statusCode == 200) {
					String link = response.body().replace("\"", "");
		 			player.sendMessage(infoMessageMessage.replace("%link%", link));
		 			return;
				} else {
		 			player.sendMessage(linkErrorMessage);
		 			return;
				}
				
			} catch (Exception e) {
				player.sendMessage(linkErrorMessage);
				e.printStackTrace();
			}	   
		};
		
		Thread thread = new Thread(runnable);
        thread.start();

        return false;        
	}
}
