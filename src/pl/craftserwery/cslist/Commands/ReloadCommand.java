package pl.craftserwery.cslist.Commands;

import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import pl.craftserwery.cslist.Main;

public class ReloadCommand implements CommandExecutor {
	 private final Main plugin;
	
    public ReloadCommand(Main plugin) {
        this.plugin = plugin;
    }
	
	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		Player player = (Player)sender;
		
		if(!player.isOp() || !player.hasPermission("cslist.admin")) {
			String requiredPermissionMessage = plugin.getConfig().getString("messages.needsPermissions");
			player.sendMessage(requiredPermissionMessage);
			return false;
		}
		try {
			this.plugin.reloadConfig();

			player.sendMessage("[CSList] Plugin został przeładowany");
		}  catch (Exception e) {
			Bukkit.getLogger().log(Level.SEVERE, e.toString());
			String errorMessage = plugin.getConfig().getString("messages.error");
			player.sendMessage(errorMessage);
			e.printStackTrace();
		}	   
        return false;        
	}
}
