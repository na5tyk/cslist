package pl.craftserwery.cslist.Commands;

import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.net.URI;
import java.net.http.HttpClient;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import pl.craftserwery.cslist.Main;

public class RewardCommand implements CommandExecutor {
	 private final Main plugin;
	Map<String, Date> cooldown = new HashMap<>();
	
    public RewardCommand(Main plugin) {
        this.plugin = plugin;
    }
	
	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		Runnable runnable = () -> {
			Player player = (Player)sender;
			String name = player.getName();
			boolean requiredPermission = plugin.getConfig().getBoolean("requiredPermission");
	
			String requiredPermissionMessage = plugin.getConfig().getString("messages.needsPermissions");
			
			if (requiredPermission && !player.hasPermission("cslist.reward")) {
	            player.sendMessage(requiredPermissionMessage);
	            return;
	        }
			
			if (cooldown.containsKey(name)) {
	
				String cooldownMessage = plugin.getConfig().getString("messages.cooldownMessage");
				
	            Date date = cooldown.get(name);
	            long dateDiff = (long) Math.floor((new Date().getTime() / 1000) - (date.getTime() / 1000));
	
	            if (dateDiff < 5) {
	                long timeLeft = 5 - dateDiff;
	                sender.sendMessage(cooldownMessage.replace("%timeLeft%", Long.toString(timeLeft)));
	
	                return;
	            }
	        }
	
			String authKey = plugin.getConfig().getString("authKey");
			
			String rewardsNotDefinedMessage = plugin.getConfig().getString("messages.rewardsNotDefined");
			String awardedMessage = plugin.getConfig().getString("messages.awarded");
			String noVotedMessage = plugin.getConfig().getString("messages.noVote");
			String noAuthKeyMessage = plugin.getConfig().getString("messages.noAuthKey");
			String alreadyVotedMessage = plugin.getConfig().getString("messages.alreadyVoted");
			
			cooldown.put(name, new Date());
			
			HttpRequest request = HttpRequest.newBuilder()
					.uri(URI.create(this.plugin.apiUrl + "/api/server/claim-reward"))
					.method("POST", HttpRequest.BodyPublishers.noBody())
					.version(HttpClient.Version.HTTP_1_1)
					.header("Content-Type", "application/json")
					.header("CS-Auth-Key", authKey)
					.header("CS-Name", name)
					.build();
			
			HttpResponse<String> response = null;
			try {
				response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
				int statusCode = response.statusCode();				
				if(statusCode == 200) {
		 			ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
		 			List<String> list = plugin.getConfig().getStringList("commands");
		 			if(list == null) {
		 				player.sendMessage(rewardsNotDefinedMessage);
		 				return;
		 			}
		 			
		 			Bukkit.getScheduler().runTask(this.plugin, () -> {
		 				for (String cmd : list) {
			 				Bukkit.dispatchCommand(console, cmd.replace("%player%", name));
						}
	 				});
		 			
		 			
		 			player.sendMessage(awardedMessage);
		 			return;
				}
				
				if(statusCode == 400) {
		 			player.sendMessage(noAuthKeyMessage);
		 			return;
				}
				
				if(statusCode == 404) {
		 			player.sendMessage(noVotedMessage);
		 			return;
				}
				
				if(statusCode == 406) {
		 			player.sendMessage(alreadyVotedMessage);
		 			return;
				}
				
			} catch (Exception e) {
				String errorMessage = plugin.getConfig().getString("messages.error");
				player.sendMessage(errorMessage);
				e.printStackTrace();
			}	   
		};
		
		Thread thread = new Thread(runnable);
        thread.start();

        return false;        
	}
}
