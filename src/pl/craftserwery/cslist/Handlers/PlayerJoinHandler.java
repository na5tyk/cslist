package pl.craftserwery.cslist.Handlers;

import pl.craftserwery.cslist.Main;
import pl.craftserwery.cslist.helpers.Version;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.google.gson.Gson;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class PlayerJoinHandler implements Listener {
	private final Main plugin;
	
    public PlayerJoinHandler(Main plugin) {
        this.plugin = plugin;
        
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        
        boolean checkUpdates = plugin.getConfig().getBoolean("checkUpdates");

        if((!player.isOp() || !player.hasPermission("cslist.admin")) && !checkUpdates) {
        	return;
        }
        
        String version = this.plugin.getDescription().getVersion();
        
        
        HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create(this.plugin.apiUrl + "/api/server/check-plugin"))
				.method("GET", HttpRequest.BodyPublishers.noBody())
				.version(HttpClient.Version.HTTP_1_1)
				.header("Content-Type", "application/json")
				.build();
		
		HttpResponse<String> response = null;
		try {
			response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
			int statusCode = response.statusCode();
			
			if(statusCode == 200) {
				Gson g = new Gson();
				Version json = g.fromJson(response.body(), Version.class);
				Bukkit.getLogger().info(version);
				Bukkit.getLogger().info(json.version);
				if(!version.equals(json.getVersion())) {
		 			player.sendMessage(json.getText());
		 			return;
				}
			}
			
		} catch (Exception e) {
			player.sendMessage("[CSList] Wystąpił błąd z sprawdzeniem wrsji pluginu");
			e.printStackTrace();
		}	
    }
}
