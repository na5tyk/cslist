package pl.craftserwery.cslist;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import pl.craftserwery.cslist.Commands.LinkCommand;
import pl.craftserwery.cslist.Commands.RewardCommand;
import pl.craftserwery.cslist.Commands.ReloadCommand;

import pl.craftserwery.cslist.Handlers.PlayerJoinHandler;

public class Main extends JavaPlugin {
	
	public String apiUrl = "https://craftserwery.pl";
	FileConfiguration config = getConfig();
	
	@Override
    public void onEnable(){
		super.onEnable();
		
		saveDefaultConfig();

		if(!config.contains("checkUpdates", true)) {
			config.addDefault("checkUpdates", true);
			config.options().copyDefaults(true);
			saveConfig();
		}

		getCommand("cs-nagroda").setExecutor(new RewardCommand(this));
		getCommand("cs-link").setExecutor(new LinkCommand(this));
		getCommand("cs-reload").setExecutor(new ReloadCommand(this));
		
	    new PlayerJoinHandler(this);
    }
}
