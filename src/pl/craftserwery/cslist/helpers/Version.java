package pl.craftserwery.cslist.helpers;

public class Version {
	public String version;
	public String text;
	
	public Version (String version, String text){
		this.version = version;
		this.text = text;
	}
	
	public String getVersion() {
		return this.version;
	}
	
	public String getText() {
		return this.text;
	}
}
